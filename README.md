# Portifolio

My Portfolio, Kelvin Silva, Hosted [Here](https://nuxtjs-kelvinportifolio.wedeploy.io/)

# Nuxt.JS

My portfolio build with [Nuxt.JS](https://nuxtjs.org/)

## Instructions

1. Clone this repository
```
$ git clone https://github.com/kfurfles/Portifolio.git 
```
2. Go to folder app.
```
cd Portifolio\app
```
3. Install dependencies
```
$ npm i 
```
4. Run project
```
$ npm run dev 
```